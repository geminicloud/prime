# Prime
Sources we use

# Courses
- www.coursera.org/learn/language-processing

# Parallel Query Engine
## Apache Storm
storm.apache.org

# List of information retrieval libraries
en.wikipedia.org/wiki/List_of_information_retrieval_libraries

- en.wikipedia.org/wiki/Lemur_Project   Top!
- en.wikipedia.org/wiki/Apache_Lucene   Top!
- en.wikipedia.org/wiki/Sketch_Engine#Manatee
- en.wikipedia.org/wiki/Terrier_Search_Engine
- en.wikipedia.org/wiki/Xapian


## Extra: Recoll
en.wikipedia.org/wiki/Recoll
I've used this before, it's Amazing!

# Data Store
## all Key-Value
- en.wikipedia.org/wiki/Key-value_database
- db-engines.com/en/ranking/key-value+store

## In-Memory
- www.voltdb.com
- en.wikipedia.org/wiki/VoltDB
- db-engines.com/en/system/Redis
- en.wikipedia.org/wiki/Redis

# Framework
## Spring Framework
Use Spring to build Unisearch & BlackCube! Since 90% of all the projects we're using are in Java!